from django.shortcuts import render, get_object_or_404
from django.views import generic
from django.contrib.auth import views as auth_views, logout
from django import http
from django.urls import reverse_lazy

from dockets.models import Docket

from .mixins import LoginRequiredMixin, ApkContextMixin
from .forms import AuthForm, CustomPasswordChangeForm, DiaryForm


class Login(auth_views.LoginView):
    template_name = "apk/login.html"
    authentication_form = AuthForm

    def get_success_url(self):
        return reverse_lazy("apk:index")

class Logout(LoginRequiredMixin, generic.View):
    def get(self, request):
        logout(request)
        return http.HttpResponseRedirect(redirect_to=self.get_login_url())


class Base(LoginRequiredMixin, ApkContextMixin):
    pass


class ChangePassword(Base, auth_views.PasswordChangeView):
    template_name = "apk/cp.html"
    form_class = CustomPasswordChangeForm

    def get_success_url(self):
        return reverse_lazy("apk:login")


class Cases(Base, generic.ListView):
    template_name = "apk/cases.html"
    context_object_name = "cases"

    def get_queryset(self):
        return self.officer.dockets_assigned.all()

class CaseDetail(Base, generic.DetailView):
    template_name = "apk/case.html"
    context_object_name = "case"
    model = Docket


class CaseDiary(Base, generic.CreateView):
    form_class = DiaryForm
    template_name = "apk/diary.html"

    def dispatch(self, request, *args, **kwargs):
        self.case = get_object_or_404(Docket, pk=kwargs.get('pk', None))
        self.initial.update({
            "docket": self.case,
        })
        self.extra_context = {"case": self.case}
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.initial.update({
            "logger": self.officer.pk,
        })
        return super().get(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy("apk:case", kwargs={"pk": self.case.pk})


class Index(Base, generic.TemplateView):
    template_name = "apk/index.html"

    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)
        c.update({
            "cases": self.officer.dockets_assigned.count(),
            "cold": self.officer.dockets_assigned.filter(cold=True).count(),
            "closed": self.officer.dockets_assigned.filter(closed=True).count(),
            "latest": self.officer.dockets_assigned.all()[:5],
        })
        return c


class DEs(Base, generic.ListView):
    template_name = 'apk/des.html'
    context_object_name = "des"

    def get_queryset(self):
        return self.officer.diary_entries.all()