from django.urls import path

from . import views

app_name = "apk"


urlpatterns = [
    path("login", views.Login.as_view(), name="login"),
    path("logout", views.Logout.as_view(), name="logout"),

    path("", views.Index.as_view(), name="index"),
    path("change-password/", views.ChangePassword.as_view(), name="change_password"),

    path("cases/", views.Cases.as_view(), name="cases"),
    path("cases/<int:pk>/", views.CaseDetail.as_view(), name="case"),
    path("cases/<int:pk>/diary/", views.CaseDiary.as_view(), name="case_diary"),
    path("diary/", views.DEs.as_view(), name="diary"),
    
    # path("tip-off/", views.TipOff.as_view(), name="tip_off"),


]