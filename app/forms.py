from django import forms
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django.conf import settings

from dockets.models import Diary

class AuthForm(AuthenticationForm):
    def confirm_login_allowed(self, user):
        if not user.is_active:
            raise forms.ValidationError(
                "This account is inactive.",
                code='inactive',
            )
        if not hasattr(user, "officer"):
            raise forms.ValidationError(
                "Access denied.", code="not_allowed"
            )

class CustomPasswordChangeForm(PasswordChangeForm):
    pass


class DiaryForm(forms.ModelForm):

    class Meta:
        model = Diary
        fields = ("docket", "notes", "logger")
        widgets = {
            "docket": forms.HiddenInput(),
            "logger": forms.HiddenInput(),
        }