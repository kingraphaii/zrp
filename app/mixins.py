from django.contrib.auth.mixins import AccessMixin
from django.urls import reverse_lazy


class LoginRequiredMixin(AccessMixin):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        else:
            if not hasattr(request.user, "officer"):
                return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

    def get_login_url(self):
        return reverse_lazy('apk:login')


class ApkContextMixin:

    def __init__(self, **kwargs):
        self.officer = None
        super().__init__(**kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['officer'] = self.officer
        return context

    def dispatch(self, request, *args, **kwargs):
        self.officer = self.request.user.officer
        return super().dispatch(request, *args, **kwargs)