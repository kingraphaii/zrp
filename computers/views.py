from django.shortcuts import render, get_object_or_404
from django.views import generic
from django.contrib.auth import views as auth_views, logout
from django.urls import reverse_lazy
from django import http

from .models import Computer, Log, Branch
from .forms import AuthForm, CustomPasswordChangeForm
from .mixins import LoginRequiredMixin

from dockets.models import Docket, Crime
from officers.models import City

class Base(LoginRequiredMixin):
    pass


class Login(auth_views.LoginView):
    template_name = "login.html"
    authentication_form = AuthForm

    def get_success_url(self):
        return reverse_lazy("com:index")


class Logout(LoginRequiredMixin, generic.View):
    def get(self, request):
        logout(request)
        return http.HttpResponseRedirect(redirect_to=self.get_login_url())


class BarChart(object):
    title = ""
    labels = []
    datasets = []

    def __init__(self, title=""):
        self.title = title
        self.labels = []
        self.datasets = []



class Index(Base, generic.TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        ctx  = super().get_context_data(**kwargs)
        
        bcb = BarChart(title="Stats By Crime")
        bcb.labels = [c.name for c in Crime.objects.all()]

        for c in Docket.objects.all():
            ds = {
                "label": 'Cases',
                "backgroundColor": '#00ffc9',
                "borderColor": '#00ffc9',
                "pointColor": '#3b8bba',
                "pointStrokeColor": 'rgba(60,141,188,1)',
                "pointHighlightFill": '#fff',
                "pointHighlightStroke": 'rgba(60,141,188,1)',
                "data" : [Docket.objects.filter(crime=c).count() for c in Crime.objects.all()], 
            }
            
            bcb.datasets.append(ds)

        ctx.update({
            "cases": Docket.objects.count(),
            "cold": Docket.objects.filter(cold=True).count(),
            "closed": Docket.objects.filter(closed=True).count(),
            "byc": bcb,
        })
        return ctx


class LogDetail(Base, generic.DetailView):
    template_name = "log.html"
    model = Log
    context_object_name = "log"


class Logs(Base, generic.ListView):
    template_name = "logs.html"
    model = Log
    context_object_name = "logs"


class Branches(Base, generic.ListView):
    model = Branch
    context_object_name = "brs"
    template_name = "branches.html"


class BranchDetail(Base, generic.DetailView):
    template_name = "branch.html"
    model = Branch
    context_object_name = "br"


class ChangePwd(Base, auth_views.PasswordChangeView):
    template_name = "cp.html"
    form_class = CustomPasswordChangeForm

    def get_success_url(self):
        return reverse_lazy("com:login")


class Computers(Base, generic.ListView):
    template_name = "comps.html"
    model = Computer
    context_object_name = "comps"


class ComputerDetail(Base, generic.DetailView):
    template_name = "comp.html"
    model = Computer
    context_object_name = "comp"




