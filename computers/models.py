from django.core.validators import ValidationError
from django.db import models

from model_utils.models import TimeStampedModel


class Branch(TimeStampedModel):
    name = models.CharField(max_length=255, help_text="e.g Teechez Gweru Main")
    
    class Meta:
        verbose_name = "branch"
        verbose_name_plural = "branches"
    
    def __str__(self):
        return self.name


class Computer(TimeStampedModel):
    TYPE_SERVER = "SERVER"
    TYPE_WORKSTATION = "WORKSTATION"
    TYPE_LAPTOP = "LAPTOP"
    COMPUTER_TYPE_CHOICES = (
        (TYPE_SERVER, "Server"),
        (TYPE_WORKSTATION, "Workstation"),
        (TYPE_LAPTOP, "Laptop"),
    )

    branch = models.ForeignKey(to="Branch", on_delete=models.CASCADE, related_name="computers")
    computer_type = models.CharField(
        max_length=255, choices=COMPUTER_TYPE_CHOICES,
    )
    serial = models.CharField(
        max_length=255, unique=True
    )
    nickname = models.CharField(
        max_length=255, help_text="e.g Blade Mail Servier/Reception Workstation"
    )


    class Meta:
        verbose_name = "computer"
        verbose_name_plural = "computers"
    
    def __str__(self):
        return '%s > %s' % (self.serial, self.branch) 
    
    @property
    def type(self):
        return self.computer_type
    
    @type.setter
    def type(self, v):
        self.computer_type = v


class Log(TimeStampedModel):
    LT_ERROR = "ERROR"
    LT_ACCESS = "ACCESS"
    LOG_TYPE_CHOICES = (
        (LT_ERROR, 'Error'),
        (LT_ACCESS, 'Access'),
    )

    computer = models.ForeignKey(
        to="Computer",
        on_delete=models.CASCADE,
        related_name="logs",
    )
    name = models.CharField(
        max_length=255, help_text="e.g Apache Access Log"
    )
    log_type = models.CharField(
        max_length=255, choices=LOG_TYPE_CHOICES,
    )

    date_from = models.DateField()
    date_to = models.DateField()

    file = models.FileField()

    class Meta:
        verbose_name = "log"
        verbose_name_plural = "logs"
    
    def __str__(self):
        return 'Log #%s from %s' % (self.id, self.computer.serial)
    
    @property
    def type(self):
        return self.log_type
    
    @type.setter
    def type(self, v):
        self.log_type = v
    
    def clean(self):
        if all([self.date_from, self.date_to]):
            if self.date_from > self.date_to:
                raise ValidationError("`Date To` cannot be greater than `Date From!`")


    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)