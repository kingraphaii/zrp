from django.urls import path, include

from . import views

app_name = "com"

urlpatterns = [
    path("", views.Index.as_view(), name="index"),
    path("logs/", views.Logs.as_view(), name="logs"),
    path("logs/<int:pk>", views.LogDetail.as_view(), name="log_detail"),

    path("computers/", views.Computers.as_view(), name="computers"),
    path("computers/<int:pk>", views.ComputerDetail.as_view(), name="computer"),

    path("branches/", views.Branches.as_view(), name="branches"),
    path("branches/<int:pk>", views.BranchDetail.as_view(), name="branch_detail"),

    path("change-password/", views.ChangePwd.as_view(), name="change_password"),
    path("login/", views.Login.as_view(), name="login"),
    path("logout/", views.Logout.as_view(), name="logout"),
]