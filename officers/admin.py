from django.contrib import admin

from .models import City, Location, Rank, Officer


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "location_count", "officers", "created", "modified")
    search_fields = ("name",)
    list_per_page = 10

@admin.register(Location)
class LoAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "city", "officer_count", "docket_count", "created", "modified")
    list_per_page = 10
    search_fields = ("name",)
    list_filter = ("city",)

@admin.register(Rank)
class RankAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "short", "created", "modified",)
    list_per_page = 10
    search_fields = ("name",)


@admin.register(Officer)
class OfAdmin(admin.ModelAdmin):
    list_display = ("force_no", "name", "sex", "age", "location", "rank", "doe", "created", "modified",)
    search_fields = ("first_name", "last_name", "force_no",)
    list_filter = ("rank__name", "sex", "location__name", "location__city__name")
    list_per_page = 10
    raw_id_fields = ("user_account", )
