import datetime

from django.db import models
from django.conf import settings

from model_utils.models import TimeStampedModel


class City(TimeStampedModel):
    name = models.CharField(max_length=255, unique=True, verbose_name="City")

    class Meta:
        verbose_name = "city"
        verbose_name_plural = "cities"

    def __str__(self):
        return self.name

    @classmethod
    def setup(cls):
        for city in ["Kwekwe", "Gweru", "Kadoma", "Harare", "Bulawayo", "Masvingo", "Gutu", "Chegutu", "Norton"]:
            c, _ = cls.objects.get_or_create(
                name=city,
            )

    @property
    def location_count(self):
        return self.locations.count()
    location_count.fget.short_description = "Locations"

    @property
    def officers(self):
        return Officer.objects.filter(
            location__city=self
        ).count()
    officers.fget.short_description = "Officers"


class Location(TimeStampedModel):
    name = models.CharField(max_length=255, verbose_name="Location")
    city = models.ForeignKey(to="City", on_delete=models.CASCADE, related_name="locations")

    class Meta:
        verbose_name = "location"
        verbose_name_plural = "locations"
        unique_together = ("name", "city")
    
    def __str__(self):
        return "%s > %s" % (self.city, self.name)

    @property
    def docket_count(self):
        return self.dockets.count()
    docket_count.fget.short_description = "Dockets"

    @property
    def officer_count(self):
        return self.officers.count()
    officer_count.fget.short_description = "Officers"


class Rank(TimeStampedModel):
    short = models.CharField(max_length=5, verbose_name="Short display format")
    name = models.CharField(max_length=255, unique=True, verbose_name="Rank")

    class Meta:
        verbose_name = "rank"
        verbose_name_plural = "ranks"
    
    def __str__(self):
        return self.name

class Officer(TimeStampedModel):
    doe = models.DateField(verbose_name="Date of Enlistment")
    
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    sex = models.CharField(max_length=1, choices=(("M", "Male"),("F", "Female")))
    dob = models.DateField()

    force_no = models.CharField(max_length=255, unique=True, verbose_name="Force No.")

    rank = models.ForeignKey(
        to="Rank",
        on_delete=models.CASCADE,
        related_name="officers",
    )

    location = models.ForeignKey(
        to="Location",
        related_name="officers",
        on_delete=models.CASCADE,
    )
    user_account = models.OneToOneField(
        to=settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True, blank=True
    )

    class Meta:
        verbose_name = "officer"
        verbose_name_plural = "officers"
    
    def __str__(self):
        return self.name
    
    @property
    def name(self):
        return "%s %s %s" % (self.rank.short, self.first_name[0], self.last_name)
    
    @property
    def age(self):
        today = datetime.date.today()
        age = today.year - self.dob.year - (
            (today.month, today.day) < (self.dob.month, self.dob.day))
        return age
    
