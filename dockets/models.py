from django.db import models

from model_utils.models import TimeStampedModel


class Court(TimeStampedModel):
    name = models.CharField(max_length=255,)
    city = models.ForeignKey(
        to="officers.City",
        related_name="courts",
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = 'court'
        verbose_name_plural = "courts"
    
    def __str__(self):
        return self.name


class Crime(TimeStampedModel):
    name = models.CharField(max_length=255, unique=True)

    class Meta:
        verbose_name = 'crime'
        verbose_name_plural = "crime categories"
    
    def __str__(self):
        return self.name

    @property
    def case_count(self):
        return self.dockets.count()
    case_count.fget.short_description = "Cases/Dockets"


class Docket(TimeStampedModel):
    crime = models.ForeignKey(
        to="Crime",
        related_name="dockets",
        on_delete=models.CASCADE,
    )
    complainant = models.CharField(
        max_length=255,
    )
    plaintiff = models.CharField(
        max_length=255, null=True, blank=True
    )
    summary = models.TextField()


    cold = models.BooleanField(default=False)
    closed = models.BooleanField(default=False)
    
    court = models.ForeignKey(
        to="Court",
        on_delete=models.CASCADE,
        related_name="cases",
        null=True, blank=True
    )
    logged_by = models.ForeignKey(
        to="officers.Officer",
        related_name="dockets_logged",
        on_delete=models.SET_NULL,
        null=True, blank=False,
    )
    location = models.ForeignKey(
        to="officers.Location",
        related_name="dockets",
        on_delete=models.CASCADE,
        null=True, blank=True, editable=False
    )
    investigators = models.ManyToManyField(
        to="officers.Officer",
        related_name="dockets_assigned",
        blank=True
    )

    class Meta:
        verbose_name = "docket"
        verbose_name_plural = "dockets"
        ordering = ("-created",)
    
    def __str__(self):
        return "Docket #%s" % self.id
    
    def clean(self):
        super().clean()
    
    def save(self, *args, **kwargs):
        self.full_clean()
        if not self.location:
            if self.logged_by:
                self.location = self.logged_by.location

        super().save(*args, **kwargs)
    
    def set_cold(self):
        self.cold = True
        self.save()


class Diary(TimeStampedModel):
    docket = models.ForeignKey(
        to="Docket",
        related_name="diary_entries",
        on_delete=models.CASCADE,
    )
    notes = models.TextField()
    logger = models.ForeignKey(
        to="officers.Officer",
        related_name="diary_entries",
        on_delete=models.CASCADE,
        null=True, blank=True,
        verbose_name="Logged By"
    )

    class Meta:
        verbose_name = 'diary entry'
        verbose_name_plural = 'diary entries'
    