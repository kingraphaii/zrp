from django.contrib import admin

from .models import Docket, Court, Crime, Diary


# Customize Header
admin.site.site_header = "ZRP E-Docket Administration Panel"
# Customize Title
admin.site.site_title = "ZRP E-Docket Administration"
# Index Title
admin.site.index_title = "ZRP E-Docket System Administration"


class DiaryInline(admin.TabularInline):
    model = Diary


@admin.register(Docket)
class DocketAdmin(admin.ModelAdmin):
    list_display = ("id", "crime", "complainant", "plaintiff", "cold", "closed", "court", "logged_by", "location", "created", "modified",)
    list_filter = ("crime", "cold", "closed", "court", "location", "location__city", "created")
    search_fields = ("id", "summary", "complainant", "plaintiff",)
    list_per_page = 20
    inlines = [DiaryInline, ]



@admin.register(Court)
class CoAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "city", "created", "modified")
    list_per_page = 10
    search_fields = ("name",)
    list_filter = ("city", )

@admin.register(Crime)
class CrAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "case_count", "created", "modified")
    search_fields = ("name",)
    list_per_page = 15
